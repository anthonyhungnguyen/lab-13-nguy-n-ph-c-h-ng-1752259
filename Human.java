package lab13;

public class Human {
	private String firstName;
	private String lastName;

	public Human() {
		this.firstName = "";
		this.lastName = "";
	}

	public Human(String firstName, String lastName) throws Exception {
		if (!Character.isUpperCase(firstName.charAt(0)))
			throw new Exception("Expected upper case letter!Argument: " + firstName);
		if (firstName.length() < 4)
			throw new Exception("Expected length at least 4 symbols!Argument: " + firstName);
		if (!Character.isUpperCase(lastName.charAt(0)))
			throw new Exception("Expected upper case letter!Argument: " + lastName);
		if (lastName.length() < 3)
			throw new Exception("Expected length at least 3 symbols!Argument: " + lastName);
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "First Name: " + firstName + "\n" + "Last Name: " + lastName + "\n";
	}
}
