package lab13;

public class Cat extends Animal {

	public Cat(String name, int age, String gender) throws Exception {
		super(name, age, gender);
	}

	public String produceSound() {
		return "MiauMiau";
	}

	public String toString() {
		return super.toString();
	}
}
