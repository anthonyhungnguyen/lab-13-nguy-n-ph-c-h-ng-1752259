package lab13;

public class Animal {
	private String name;
	private int age;
	private String gender;

	public Animal() {
	}

	public Animal(String name, int age, String gender) throws Exception {
		if (age < 0 || name.length() == 0 || age == 0 || gender.length() == 0)
			throw new Exception("Invalid input");
		this.name = name;
		this.age = age;
		this.gender = gender;
	}

	public String produceSound() {
		return "Not implemented";
	}

	public String toString() {
		return name + " " + age + " " + gender;
	}

}
