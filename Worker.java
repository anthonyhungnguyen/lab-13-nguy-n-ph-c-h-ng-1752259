package lab13;

public class Worker extends Human {
	private double weekSalary;
	private double workHoursPerDay;

	public Worker() {
	}

	public Worker(String firstName, String lastName, double weekSalary, double workHoursPerDay) throws Exception {
		super(firstName, lastName);
		if (weekSalary <= 10)
			throw new Exception("Expected value mismatch!Argument: " + weekSalary);
		if (workHoursPerDay < 1 || workHoursPerDay > 12)
			throw new Exception("Expected value mismatch!Argument: " + workHoursPerDay);
		this.weekSalary = weekSalary;
		this.workHoursPerDay = workHoursPerDay;
	}

	public double salaryPerHour() {
		return weekSalary / (workHoursPerDay * 7);
	}

	public double getWeekSalary() {
		return weekSalary;
	}

	public void setWeekSalary(double weekSalary) {
		this.weekSalary = weekSalary;
	}

	public double getWorkHoursPerDay() {
		return workHoursPerDay;
	}

	public void setWorkHoursPerDay(int workHoursPerDay) {
		this.workHoursPerDay = workHoursPerDay;
	}

	public String toString() {
		return super.toString() + "Week Salary: " + String.format("%.2f", weekSalary) + "\n" + "Hours per day: "
				+ String.format("%.2f", workHoursPerDay) + "\n" + "Salary per hour: "
				+ String.format("%.2f", salaryPerHour()) + "\n";
	}
}
