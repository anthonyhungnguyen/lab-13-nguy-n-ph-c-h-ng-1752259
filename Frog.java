package lab13;

public class Frog extends Animal {

	public Frog(String name, int age, String gender) throws Exception {
		super(name, age, gender);

	}

	public String produceSound() {
		return "Frogggg";
	}

}
