package lab13;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws Exception {

		// Scanner sc = new Scanner(System.in);
		// String studentFirstName = sc.next();
		// String studentLastName = sc.next();
		// String facultyNumber = sc.next();
		// String workerFirstName = sc.next();
		// String workerLastName = sc.next();
		// double weekSalary = sc.nextDouble();
		// int workHoursPerDay = sc.nextInt();

		// Student student = new Student(studentFirstName, studentLastName,
		// facultyNumber);
		// System.out.println(student);
		// Worker worker = new Worker(workerFirstName, workerLastName, weekSalary,
		// workHoursPerDay);
		// System.out.println(worker);

		// sc.close();

		Scanner sc = new Scanner(System.in);
		String type = sc.next();
		while (!type.equals("Beast!")) {
			String name = sc.next();
			int age = sc.nextInt();
			String gender = sc.next();
			switch (type) {
			case "Cat":
				Cat c = new Cat(name, age, gender);
				System.out.println(type);
				System.out.println(c.toString());
				System.out.println(c.produceSound());
				break;
			case "Dog":
				Dog d = new Dog(name, age, gender);
				System.out.println(type);
				System.out.println(d.toString());
				System.out.println(d.produceSound());
				break;
			case "Frog":
				Frog f = new Frog(name, age, gender);
				System.out.println(type);
				System.out.println(f.toString());
				System.out.println(f.produceSound());
				break;
			case "Kitten":
				Kitten k = new Kitten(name, age, gender);
				System.out.println(type);
				System.out.println(k.toString());
				System.out.println(k.produceSound());
				break;
			case "Tomcat":
				Tomcat t = new Tomcat(name, age, gender);
				System.out.println(type);
				System.out.println(t.toString());
				System.out.println(t.produceSound());
				break;
			default:
				Animal a = new Animal();
				System.out.println(a.produceSound());
			}
			type = sc.next();
		}
		sc.close();
	}

}
