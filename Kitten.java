package lab13;

public class Kitten extends Cat {

	public Kitten(String name, int age, String gender) throws Exception {
		super(name, age, "Female");
		if (!gender.equals("Female"))
			throw new Exception("Invalid input");
	}

	public String produceSound() {
		return "Miau";
	}

}
